## Usage

After creating all the data resources (`make all`). There is one last step that
is currently manual:

1. Find the hash for the latest quilt data package that you just generated.
   ```bash
   quilt log equilibrator/cache
   ```
2. Use that hash to create a new version.
   ```bash
   quilt version add equilibrator/cache <version> <hash>
   ```
3. Update `DEFAULT_QUILT_VERSION` in `equilibrator_cache/__init__.py` to the new
   version string and make a new release of that package.
