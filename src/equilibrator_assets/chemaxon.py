# The MIT License (MIT)
#
# Copyright (c) 2013 The Weizmann Institute of Science.
# Copyright (c) 2018 Institute for Molecular Systems Biology, ETH Zurich.
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


"""Enrich compounds with cheminformatic properties."""


import logging
from collections import namedtuple
from typing import Dict, List, Tuple

import rdkit
from openbabel import pybel
from periodictable import elements
from pkasolver.query import QueryModel, calculate_microstate_pka_values
from rdkit.Chem import MolFromInchi, MolFromSmiles, MolToSmiles
from rdkit.Chem.Descriptors import MolWt
from rdkit.Chem.MolStandardize import rdMolStandardize
from rdkit.Chem.rdchem import Mol


ATOMIC_NUMBER_TO_SYMBOL = {e.number: e.symbol for e in elements}
SYMBOL_TO_ATOMIC_NUMBER = {e.symbol: e.number for e in elements}
QUERY_MODEL = QueryModel()

logger = logging.getLogger(__name__)
rdkit.rdBase.LogToPythonLogger()
rdlogger = logging.getLogger("rdkit")
rdlogger.setLevel(rdkit.RDLogger.DEBUG)
rdkit.RDLogger.DisableLog("rdApp.*")
rdkit.RDLogger.DisableLog("rdApp.debug")
rdkit.RDLogger.DisableLog("rdApp.error")
rdkit.RDLogger.DisableLog("rdApp.warning")
rdkit.RDLogger.DisableLog("rdApp.info")


MoleculeData = namedtuple("MoleculeData", ["atom_bag", "smiles"])
# A dictionary from InChIKey to MoleculeData objects
# Here we list a few exceptions, i.e. compounds that are not treated
# correctly by cxcalc, and override them with our own data
COMPOUND_EXCEPTIONS = {
    # H+
    # ChemAxon fails if we try to run it on a single proton.
    # error is "pka: Inconsistent molecular structure."
    "InChI=1S/p+1": MoleculeData(atom_bag={"H": 1}, smiles=None),
    # sulfur
    # ChemAxon gets confused with the structure of sulfur
    #  (returns a protonated form, [SH-], at pH 7).
    "InChI=1S/S": MoleculeData(atom_bag={"S": 1, "e-": 16}, smiles="S"),
    # CO
    # ChemAxon gets confused with the structure of carbon
    # monoxide (returns a protonated form, [CH]#[O+], at pH 7).
    "InChI=1S/CO/c1-2": MoleculeData(
        atom_bag={"C": 1, "O": 1, "e-": 14}, smiles="[C-]#[O+]"
    ),
    # H2
    "InChI=1S/H2/h1H": MoleculeData(atom_bag={"H": 2, "e-": 2}, smiles=None),
    # Metal Cations get multiple pKa values from ChemAxon, which is
    # obviously a bug. We override the important ones here:
    # Ca2+
    "InChI=1S/Ca/q+2": MoleculeData(atom_bag={"Ca": 1, "e-": 18}, smiles="[Ca++]"),
    # K+
    "InChI=1S/K/q+1": MoleculeData(atom_bag={"K": 1, "e-": 18}, smiles="[K+]"),
    # Mg2+
    "InChI=1S/Mg/q+2": MoleculeData(atom_bag={"Mg": 1, "e-": 10}, smiles="[Mg++]"),
    # Fe2+
    "InChI=1S/Fe/q+2": MoleculeData(atom_bag={"Fe": 1, "e-": 24}, smiles="[Fe++]"),
    # Fe3+
    "InChI=1S/Fe/q+3": MoleculeData(atom_bag={"Fe": 1, "e-": 23}, smiles="[Fe+++]"),
    # thiocyanate
    "InChI=1S/CHNS/c2-1-3/h3H/p-1": MoleculeData(
        atom_bag={"S": 1, "C": 1, "N": 1, "H": 1, "e-": 31}, smiles="[S-]C#N"
    ),
    # These following compounds don't have a real structure, but we still
    # want to use them for training the component-contribution data
    # ferredoxin(red)
    "metanetx.chemical:MNXM169": MoleculeData(
        atom_bag={"Fe": 1, "e-": 26}, smiles=None
    ),
    # ferredoxin(ox)
    "metanetx.chemical:MNXM178": MoleculeData(
        atom_bag={"Fe": 1, "e-": 25}, smiles=None
    ),
}


def parse_molecule_representation(
    molecule_representation_string: str,
) -> [rdkit.Chem.rdchem.Mol, None]:
    """Parse a molecule representation.

    When entering SMILES or InChI, this function tries to detect InChI or SMILES
    string, and to parse it. No error-detection behavior so far.

    Parameters
    ----------
    molecule_representation_string: str
        An InChI or SMILES which intends to represent the molecule under consideration.

    Returns
    ----------
    molecule: rdkit.Chem.rdchem.Mol, None
        A rdkit.Chem.rdchem.Mol, or None if molecule_representation_string could not be
        parsed.
    """
    logger.debug(f"Trying to parse the string: '{molecule_representation_string}'")
    if molecule_representation_string.startswith("InChI=1S"):
        logger.debug("Try to parse as Standard InChI...")
        molecule = MolFromInchi(inchi=molecule_representation_string)
    elif molecule_representation_string.startswith("InChI=1"):
        logger.debug("Try to parse as Non-Standard InChI...")
        molecule = MolFromInchi(inchi=molecule_representation_string)
    else:
        logger.debug("Try to parse as SMILES...")
        molecule = MolFromSmiles(SMILES=molecule_representation_string)
    return molecule


def standardize_molecule_representation(
    molecule_representation_string: str,
) -> [Mol, None]:
    """
    Parse a molecule representation string and generate a standardized molecule object.

    When entering SMILES or InChI, there may be
    multiple equivalent ways of writing the same compound. eQuilibrator does not take
    charge, isotopes, tautomerization, aromaticity (Kekule form or not) into account
    for differentiating between compounds. Multiple unconnected molecules in one
    representation are not allowed. This function returns a rdkit.Chem.rdchem.Mol
    object which does not contain either of the above-mentioned information.

    Parameters
    ----------
    molecule_representation_string: str
        An InChI or SMILES which intends to represent the molecule under consideration.

    Returns
    ----------
    standardized_molecule: rdkit.Chem.rdchem.Mol, None
        A rdkit.Chem.rdchem.Mol which is a standardized representation of the molecule.
        Returns None if molecule_representation_string could not be parsed.
    """
    logger.debug(
        f"Trying to standardize the string: '{molecule_representation_string}'"
    )
    molecule = parse_molecule_representation(molecule_representation_string)
    if molecule is None:
        logger.error(
            f"Failed to parse '{molecule_representation_string}', cannot continue."
        )
        return None
    logger.debug("Successfully parsed.")

    # TODO: removal of e.g. sodium ions should be allowed
    # NOTE: skipStandardize is important, because otherwise the fragment won't match below
    fragment_parent = rdMolStandardize.FragmentParent(molecule, skipStandardize=True)
    if molecule.GetNumAtoms() != 0:
        # "molecule.GetNumAtoms() != 0" is a workaround: it catches the No-Molecule ...
        # (No Atoms => still a valid SMILES!)
        if not (
            molecule.HasSubstructMatch(fragment_parent)
            and fragment_parent.HasSubstructMatch(molecule)
        ):
            raise Exception(
                "You submitted a molecule_representation_string which contains "
                "multiple unconnected molecules. As I cannot decide which one is "
                "important I will raise an Exception."
            )
    molecule = rdMolStandardize.ChargeParent(molecule)
    molecule = rdMolStandardize.IsotopeParent(molecule)
    molecule = rdMolStandardize.TautomerParent(molecule)
    return molecule


def get_molecular_mass(molecule_representation_string: str) -> float:
    """
    Compute the average molecular mass.

    Parameters
    ----------
    molecule_representation_string: str
    An InChI or SMILES which intends to represent the molecule under consideration.


    Returns
    -------
    mol_mass : float
        The average molecular mass of the molecule.

    """
    molecule = standardize_molecule_representation(molecule_representation_string)
    return MolWt(molecule)


def get_dissociation_constants(
    molecule_representation_string: str,
) -> Tuple[str, List[float]]:
    """
    Compute the proton dissociation constants, and major microspecies at pH=7.0.

    Parameters
    ----------
        molecule_representation_string: str
        An InChI or SMILES which intends to represent the molecule under consideration.

    Returns
    -------
    major_ms : str

    pkas : List[float]
        result_df - the input molecules data frame joined with the results, found in the
        newly added columns 'major_ms' (containing the representation of the major
        microspecies as SMILES string) as well as in the columns 'apKa{i}' and
        'bpKa{i}' (see pka_columns below).
        pka_columns - a list of the column names in 'view' containing pKa values,
        usually along the lines of ['apKa1', 'apKa2', ..., 'bpKa1', 'bpKa2', ...], each
        up to the number of 'num_acidic' and 'num_basic' you provided.

    """
    mol = standardize_molecule_representation(molecule_representation_string)
    protonation_states = calculate_microstate_pka_values(
        mol, only_dimorphite=False, query_model=QUERY_MODEL
    )
    if len(protonation_states) > 0:
        major_microspecies_mol = protonation_states[0].ph7_mol
    else:
        major_microspecies_mol = mol

    major_ms = MolToSmiles(major_microspecies_mol)
    pkas = list(sorted([state.pka for state in protonation_states]))

    return major_ms, pkas


def get_atom_bag(
    mol_format: str, molecule_representation_string: str
) -> Dict[str, int]:
    """
    Compute the atom bag and the formal charge of a molecule.

    The formal charge is calculated by summing the formal charge of each atom
    in the molecule.

    Parameters
    ----------
    mol_format : str
        The format in which the molecule is given ("inchi", "smi", etc.)
    molecule_representation_string : str
        The molecular descriptor.

    Returns
    -------
    dict
        A dictionary of atom counts.

    """
    # TODO: can we use RDKit instead of openbabel here?

    molecule = pybel.readstring(mol_format, molecule_representation_string)
    # Make all hydrogen atoms explicit so we can properly count them
    molecule.addh()

    # Count charges and atoms.
    atom_bag = dict()
    formal_charge = 0
    for atom in molecule.atoms:
        symbol = ATOMIC_NUMBER_TO_SYMBOL[atom.atomicnum]
        atom_bag[symbol] = atom_bag.get(symbol, 0) + 1
        formal_charge += atom.formalcharge

    # count all protons (from all types of atoms) in the molecule
    total_num_protons = sum(
        count * SYMBOL_TO_ATOMIC_NUMBER[elem] for elem, count in atom_bag.items()
    )

    # protons are the only positively charged particles, and electrons
    # are the only negatively charged ones, so the formal_charge is
    # exactly equal to the difference between these two counts.
    atom_bag["e-"] = total_num_protons - formal_charge
    return atom_bag
