"""Tests for ChemAxon."""
# The MIT License (MIT)
#
# Copyright (c) 2020 Institute for Molecular Systems Biology, ETH Zurich.
# Copyright (c) 2020 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import logging

import numpy as np
import pandas as pd
import pytest
from rdkit.Chem import MolToSmiles

from equilibrator_assets.chemaxon import (
    get_atom_bag,
    get_dissociation_constants,
    get_molecular_mass,
    standardize_molecule_representation,
)


logger = logging.getLogger(__name__)
logger.setLevel(logging.WARNING)


@pytest.fixture(scope="module")
def molecules() -> pd.DataFrame:
    """Create a small compound table for testing ChemAxon and OpenBabel."""
    inchis = pd.Series(
        [  # adenine
            "InChI=1S/C5H5N5/c6-4-3-5(9-1-7-3)10-2-8-4/h1-2H,(H3,6,7,8,9,10)",
            "InChI=1S/C2H4O2/c1-2(3)4/h1H3,(H,3,4)/p-1",  # acetate
            "InChI=1S/C6H6/c1-2-4-6-5-3-1/h1-6H",  # benzene
        ]
    )
    inchis.name = "inchi"

    res = pd.DataFrame(data=inchis)
    res.index.name = "id"
    return res.reset_index()


@pytest.mark.parametrize(
    "smiles, expected_atom_bag",
    [
        ("CC=O", {"C": 2, "O": 1, "H": 4, "e-": 24}),
        ("C#[N-]", {"C": 1, "H": 1, "N": 1, "e-": 15}),
    ],
)
def test_atom_bag(smiles, expected_atom_bag):
    """Test the OpenBabel based calculation of atom bag and net charge."""
    atom_bag = get_atom_bag("smi", smiles)
    assert atom_bag == expected_atom_bag


@pytest.mark.parametrize(
    "structures",
    [
        (
            {
                "CCO",
                # "[13C]CO",
                "CC[O-]",
            }
        ),
        (
            {
                "C(C(=O)O)N",
                "C(C(=O)[O-])[NH3+]",
                # "C(C(=O)O)N.Cl",
                # "[NH3+]CC([O-])[O-]",
                # "NCC([O-])[O-]",
                "[NH3+]CC(O)=O",
            }
        ),
        (
            {
                "NC1=NC=NC2=C1N=CN2",
                "NC1=C2N=CNC2=NC=N1",
                "[H][N+]1=C(N)C2=C(NC=N2)N=C1",
                "[NH3+]C1=NC=NC2=C1N=CN2",
                "NC1=NC=NC2=C1[N-]C=N2",
                "NC1=C2N=CN=C2NC=N1",
                "N=C1NC=NC2=C1N=CN2",
            }
        ),
    ],
)
def test_standardization(structures):
    """Test the standardization procedure."""
    results = []
    for s in structures:
        mol = standardize_molecule_representation(s)
        smiles = MolToSmiles(mol)
        results.append(smiles)
    assert len(set(results)) == 1


@pytest.mark.parametrize(
    "structure, expected_mass",
    [
        ("InChI=1S/C5H5N5/c6-4-3-5(9-1-7-3)10-2-8-4/h1-2H,(H3,6,7,8,9,10)", 135.1),
        # adenine
        ("InChI=1S/C2H4O2/c1-2(3)4/h1H3,(H,3,4)/p-1", 60.1),  # acetate
        ("InChI=1S/C6H6/c1-2-4-6-5-3-1/h1-6H", 78.1),  # benzene
    ],
)
def test_molecular_mass(structure, expected_mass):
    """Test the ChemAxon-based calculation of molecular mass."""
    mass = get_molecular_mass(structure)
    assert mass == pytest.approx(expected_mass, rel=1e-3)


@pytest.mark.parametrize(
    "structure, expected_major_ms, expected_pkas",
    [
        (
            "InChI=1S/C5H5N5/c6-4-3-5(9-1-7-3)10-2-8-4/h1-2H,(H3,6,7,8,9,10)",
            "Nc1ncnc2[nH]cnc12",
            [2.76, 4.25, 5.12, 5.47, 8.51, 9.24],
        ),
        # adenine
        ("InChI=1S/C2H4O2/c1-2(3)4/h1H3,(H,3,4)/p-1", "CC(=O)[O-]", [4.19]),  # acetate
        ("InChI=1S/C6H6/c1-2-4-6-5-3-1/h1-6H", "c1ccccc1", []),  # benzene
    ],
)
def test_dissociation_constants(structure, expected_major_ms, expected_pkas):
    """Test the ChemAxon-based calculation of dissociation constants."""
    major_ms, pkas = get_dissociation_constants(structure)
    assert major_ms == expected_major_ms
    np.testing.assert_allclose(pkas, expected_pkas, atol=1e-2)
