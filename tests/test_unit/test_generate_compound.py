"""Tests for generating compounds."""
# The MIT License (MIT)
#
# Copyright (c) 2020 Institute for Molecular Systems Biology, ETH Zurich.
# Copyright (c) 2020 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import numpy as np
import pytest

from equilibrator_assets.generate_compound import get_or_create_compound


@pytest.mark.parametrize(
    "structure, expected_pkas, expected_num_pkmgs",
    [
        (
            "InChI=1S/C5H5N5/c6-4-3-5(9-1-7-3)10-2-8-4/h1-2H,(H3,6,7,8,9,10)",
            [9.84, 2.51],
            2,
        ),
        # adenine
        ("InChI=1S/C2H4O2/c1-2(3)4/h1H3,(H,3,4)/p-1", [4.54], 1),  # acetate
        ("InChI=1S/C6H6/c1-2-4-6-5-3-1/h1-6H", [], 0),  # benzene
    ],
)
def test_get_from_inchis(
    structure, expected_pkas, expected_num_pkmgs, comp_contribution
) -> None:
    """Create a small compound table for testing ChemAxon and OpenBabel."""
    compound = get_or_create_compound(
        comp_contribution.ccache, mol_strings=structure, mol_format="inchi"
    )

    np.testing.assert_allclose(
        compound.dissociation_constants, expected_pkas, atol=1e-2
    )
    assert len(compound.magnesium_dissociation_constants) == expected_num_pkmgs


@pytest.mark.parametrize(
    "structure, expected_pkas",
    [
        (  # 3-Aminobutane-1,2,4-triol
            "OCC(N)C(O)CO",
            [10.01, 9.79, 9.53, 8.90],
        ),
        (  # propyl-phosphate
            "CCCOP(=O)(O)O",
            [2.60],
        ),
    ],
)
def test_create_from_smiles(structure, expected_pkas, comp_contribution) -> None:
    """Create a compound that is not in the cache."""
    compound = get_or_create_compound(
        comp_contribution.ccache,
        mol_strings=structure,
        mol_format="smiles",
    )
    np.testing.assert_allclose(
        compound.dissociation_constants, expected_pkas, atol=1e-2
    )

    assert compound.id < 0
    assert len(compound.magnesium_dissociation_constants) == 0
    assert compound.group_vector is not None
