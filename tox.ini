[tox]
envlist = isort, black, flake8, safety, py{311}

[testenv]
setenv =
    PYTHONUNBUFFERED=1
deps =
    pytest
    pytest-cov
    pytest-mock
    pytest-raises
    torch==2.1.1
allowlist_externals =
    cxcalc
    cxcalc.bat
commands =
    pytest --cov=equilibrator_assets {posargs:} {toxinidir}/tests

[testenv:black]
skip_install = True
deps=
    black
commands=
    black --line-length 88 --check --diff {toxinidir}/src/equilibrator_assets {toxinidir}/tests

[testenv:isort]
skip_install = True
deps=
    isort
commands=
    isort --check --diff {toxinidir}/src/equilibrator_assets {toxinidir}/tests

[testenv:flake8]
skip_install = True
deps=
    flake8
    flake8-docstrings
    flake8-bugbear
commands=
    flake8 {toxinidir}/src/equilibrator_assets {toxinidir}/tests

[testenv:safety]
deps=
    safety
    torch==1.13.1
commands=
    safety check --ignore 51668

[testenv:codecov]
passenv=
    GITLAB_CI CI_BUILD_REF_NAME CI_BUILD_ID CI_BUILD_REPO CI_BUILD_REF CODECOV_TOKEN
deps=
    codecov
commands=
    codecov

################################################################################
# Testing tools configuration                                                  #
################################################################################

[flake8]
max-line-length = 88
exclude =
    __init__.py
    _version.py
# The following conflict with `black` which is the more pedantic.
ignore =
    E203
    W503
    D202

[isort]
skip = __init__.py
line_length = 88
indent = 4
multi_line_output = 3
include_trailing_comma = true
force_grid_wrap = 0
use_parentheses = true
lines_after_imports = 2
known_first_party = equilibrator_assets
known_third_party =
    click
    click_log
    component_contribution
    dateutil
    equilibrator_cache
    importlib_resources
    numpy
    openbabel
    pandas
    pybel
    pytest
    pytest-mock
    quilt
    requests
    scipy
    sqlalchemy
    tqdm
    whoosh
    pint
    periodictable

[coverage:paths]
source =
    src/equilibrator_assets
    */site-packages/equilibrator_assets

[coverage:run]
branch = true
parallel = true
omit =
    src/equilibrator_assets/_version.py

[coverage:report]
precision = 2
omit =
    src/equilibrator_assets/_version.py
